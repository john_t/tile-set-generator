Tile Set Generator
==================

Tilesets are a pain to make.

This project aims to make this moderately easier. Rather than having to
make 36 individual tiles, you only need to make 6! This project runs on
a directory that looks like `The test directory <example/testset>`__,
minus the ``out.png`` file which is an example out.

Install via pip::

    pip3 install tilesetgenerator
